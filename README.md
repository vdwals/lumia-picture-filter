Lumia Picture Filter
=========

Some time ago, the windows lumia camera created a few separate picture files, that were storing further information only usable on the phone itself. I created this program to filter and delete or zip those files to keep my picture folders clean.