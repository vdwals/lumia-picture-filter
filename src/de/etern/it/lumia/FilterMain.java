/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.lumia;

import de.etern.it.bugReport.BugReport;
import de.etern.it.console.Console;
import de.etern.it.progressMonitor.CommonProgressMonitor;
import de.etern.it.progressMonitor.Counter;
import de.etern.it.progressMonitor.Progress;
import de.etern.it.properties.Property;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 *
 */

/**
 * Sorgt f�r eine zuf�llige Auflistung aller Lieder in einem Verzeichnis, indem
 * die urspr�ngliche Nummerierung durch eine zuf�llige ersetzt wird.
 *
 * @author vanderwals
 */
public class FilterMain {
    private static final String FILE_EXTENSION_COMPRESS[] = {".tnl", ".nar"};
    private static final String FILE_EXTENSION_TO_MANIPULATE = ".thm";
    private static final String FILE_EXTENSION_VIDEO = ".mp4";
    private static final String FILE_EXTENSION_PICTURE = ".jpg";
    private static final String FILE_EXTENSION_HIGHRES = "__highres.jpg";
    private static final String FILE_EXTENSION_ARCHIVE[] = {".tna", ".naa"};

    private JFrame frame;
    private JTextField path;
    private JRadioButton deleteHighRes, deleteLowRes, deleteNone, convertLivingImagesToVideo, restoreLivingImages, doNothingOnLivingImages, doNothingOnRefocus, archiveRefocus, unarchiveRefocus;
    private JCheckBox subfolders;
    private JButton selectDir, start;

    private ActionListener buttonActions;

    private HashMap<File, LinkedList<File>> lowResPictures;
    private HashMap<File, LinkedList<File>> highResPictures;
    private HashMap<File, LinkedList<File>> tnlFiles;
    private HashMap<File, LinkedList<File>> thmFiles;
    private HashMap<File, LinkedList<File>> narFiles;
    private HashMap<File, LinkedList<File>> videos;
    private HashMap<File, LinkedList<File>> tnlArchives;
    private HashMap<File, LinkedList<File>> narArchives;
    private File directory;

    /**
     * Creates the GUI
     */
    private FilterMain() {
        JPanel topPanel = new JPanel(new BorderLayout(5, 5));
        topPanel.add(new JLabel(Property.getString("dir")), BorderLayout.WEST);
        topPanel.add(getPath(), BorderLayout.CENTER);
        topPanel.add(getSelectDir(), BorderLayout.EAST);

        JPanel livingImagesOptions = new JPanel(new GridLayout(1, 0, 5, 5));
        livingImagesOptions.setBorder(new TitledBorder(Property.getString("livingImagesTitle")));
        livingImagesOptions.add(getConvertLivingImagesToVideo());
        livingImagesOptions.add(getRestoreLivingImages());
        livingImagesOptions.add(getDoNothingOnLivingImages());

        JPanel resolutionFilterOptions = new JPanel(new GridLayout(1, 0, 5, 5));
        resolutionFilterOptions.setBorder(new TitledBorder(Property.getString("resolutionFilter")));
        resolutionFilterOptions.add(getDeleteLowRes());
        resolutionFilterOptions.add(getDeleteHighRes());
        resolutionFilterOptions.add(getDeleteNone());

        JPanel refocusOptions = new JPanel(new GridLayout(1, 0, 5, 5));
        refocusOptions.setBorder(new TitledBorder(Property.getString("refocus")));
        refocusOptions.add(getArchiveRefocus());
        refocusOptions.add(getUnarchiveRefocus());
        refocusOptions.add(getDoNothingOnRefocus());

        ButtonGroup group1 = new ButtonGroup();
        group1.add(getDeleteHighRes());
        group1.add(getDeleteLowRes());
        group1.add(getDeleteNone());

        ButtonGroup group2 = new ButtonGroup();
        group2.add(getConvertLivingImagesToVideo());
        group2.add(getRestoreLivingImages());
        group2.add(getDoNothingOnLivingImages());

        ButtonGroup group3 = new ButtonGroup();
        group3.add(getArchiveRefocus());
        group3.add(getUnarchiveRefocus());
        group3.add(getDoNothingOnRefocus());

        JPanel options = new JPanel(new GridLayout(4, 0, 5, 5));
        options.add(getSubfolders());
        options.add(livingImagesOptions);
        options.add(resolutionFilterOptions);
        options.add(refocusOptions);

        JPanel button = new JPanel(new BorderLayout(5, 5));
        button.add(getStart(), BorderLayout.WEST);

        JPanel top = new JPanel(new BorderLayout(5, 5));
        top.add(topPanel, BorderLayout.NORTH);
        top.add(options, BorderLayout.CENTER);
        top.add(button, BorderLayout.SOUTH);

        getFrame().getContentPane().setLayout(new BorderLayout(5, 5));
        getFrame().getContentPane().add(top, BorderLayout.NORTH);
        getFrame().getContentPane().add(new JScrollPane(Console.getInstance().getOutput()), BorderLayout.CENTER);
        getFrame().getContentPane().add(CommonProgressMonitor.INSTANCE.getPanel(), BorderLayout.SOUTH);

        getFrame().setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getFrame().pack();
        getFrame().setLocation((Toolkit.getDefaultToolkit().getScreenSize().width - getFrame().getWidth()) / 2, (Toolkit.getDefaultToolkit().getScreenSize().height - getFrame().getHeight()) / 2);
        getFrame().setVisible(true);
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // System.out.println(Pattern.matches(PATTERN_START + PATTERN_ALG,
        // "00 - Tom Odell"));
        // System.out.println(Pattern.matches(PATTERN_START + PATTERN_ALG,
        // "Tom Odell"));
        //
        // System.out.println("00 - Tom Odell".replaceAll(PATTERN_START, ""));
        // Design laden
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e1) {
            BugReport.recordBug(e1, true);
        }

        Property.PACKET = "de.etern.it.lumia";
        Property.RESOURCE = false;
        Console.getInstance().printTimeStamp = false;
        Console.getInstance().printLog = false;
        CommonProgressMonitor.INSTANCE.start();

        new FilterMain();
    }

    private static void deleteFiles(LinkedList<File> files) {
        for (File f : files) {
            f.delete();
            Console.getInstance().write(Property.getString("deleted") + ": " + f.getAbsolutePath());
        }
    }

    private static LinkedList<File> unpackIt(File zipFile, String outputFolder) throws IOException {
        Console.getInstance().write(Property.getString("unarchive") + ": " + zipFile.getAbsolutePath());

        LinkedList<File> unarchivedFiles = new LinkedList<>();

        byte[] buffer = new byte[1024];

        //create output directory is not exists
        File folder = new File(outputFolder);
        if (!folder.exists()) {
            folder.mkdir();
        }

        //get the zip file content
        ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
        //get the zipped file list entry
        ZipEntry ze = zis.getNextEntry();

        while (ze != null) {

            String fileName = ze.getName();
            File newFile = new File(buildPath(outputFolder, fileName));

            Progress unzipFile = CommonProgressMonitor.INSTANCE.createProgress(Property.getString("unarchive") + ": " + newFile.getAbsolutePath(), false, false, false, false);
            unzipFile.setMax((int) ze.getCompressedSize() / 1000);
            unzipFile.start();

            Console.getInstance().write(Property.getString("unarchive") + ": " + newFile.getAbsolutePath());
            FileOutputStream fos = new FileOutputStream(newFile);

            int len;
            while ((len = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);

                unzipFile.increment();
            }

            unzipFile.finish();

            fos.close();
            unarchivedFiles.add(newFile);
            ze = zis.getNextEntry();
        }

        zis.closeEntry();
        zis.close();

        return unarchivedFiles;
    }

    private static void packZip(File output, LinkedList<File> sources) throws IOException {
        Console.getInstance().write(Property.getString("create") + ": " + output.getAbsolutePath());
        ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(output));
        zipOut.setLevel(Deflater.DEFAULT_COMPRESSION);

        Progress zipFiles = CommonProgressMonitor.INSTANCE.createProgress(Property.getString("compress") + " " + Property.getString("dir"), false, false, false, false);
        zipFiles.setMax(sources.size());
        zipFiles.start();

        for (File source : sources) {
            zipFile(zipOut, source);

            zipFiles.increment();
        }

        zipFiles.finish();

        zipOut.flush();
        zipOut.close();
    }

    private static String buildPath(String path, String file) {
        if (path == null || path.isEmpty())
            return file;

        else
            return path + File.separator + file;
    }

    private static void zipFile(ZipOutputStream zos, File file) throws IOException {
        if (!file.canRead()) {
            Console.getInstance().write(file.getCanonicalPath() + Property.getString("cannotReadFile"));
            return;
        }

        Console.getInstance().write(Property.getString("compress") + ": " + file.getAbsolutePath());
        zos.putNextEntry(new ZipEntry(buildPath("", file.getName())));

        FileInputStream fis = new FileInputStream(file);

        Progress zipFile = CommonProgressMonitor.INSTANCE.createProgress(Property.getString("compress") + ": " + file.getAbsolutePath(), false, false, false, false);
        zipFile.setMax((int) file.length() / 1000);
        zipFile.start();

        byte[] buffer = new byte[4092];
        int byteCount;
        while ((byteCount = fis.read(buffer)) != -1) {
            zos.write(buffer, 0, byteCount);
            zipFile.addValue(4);
        }
        zipFile.finish();

        fis.close();
        zos.closeEntry();
    }

    /**
     * Verarbeitet das angegebene Verzeichnis und identifiziert die Dateien.
     */
    private void editFiles(File dir) throws IOException {
        if (!dir.canRead()) {
            Console.getInstance().write(dir.getCanonicalPath() + Property.getString("cannotReadDir"));
            return;
        }

        LinkedList<File> directories = new LinkedList<>();
        directories.add(dir);

        dir = null;

        String s = Property.getString("analyse1");
        if (getSubfolders().isSelected())
            s = Property.getString("analyse2");

        Progress analyse = CommonProgressMonitor.INSTANCE.createProgress(s, true, false, false, false);
        Counter files = analyse.createCounter(Property.getString("files") + " " + Property.getString("found"), null);
        Counter files2 = analyse.createCounter(Property.getString("files") + " " + Property.getString("analysed"), null);
        analyse.start();

        // Bestimme Anzahl der Ordner und Dateien
        while (!directories.isEmpty()) {
            File tempDir = directories.pop();
            Console.getInstance().write(Property.getString("actual") + ": " + tempDir.getAbsolutePath());

            for (String file : tempDir.list()) {
                files2.increment();
                File tempFile = new File(tempDir.getAbsolutePath() + "/" + file);

                if (tempFile.isDirectory() && getSubfolders().isSelected()) {
                    directories.push(tempFile);
                    files.increment();

                } else if (file.endsWith(FILE_EXTENSION_PICTURE) && !getDeleteNone().isSelected()) {
                    if (file.endsWith(FILE_EXTENSION_HIGHRES))
                        putHighResPictures(tempDir, tempFile);
                    else
                        putLowResPictures(tempDir, tempFile);
                    //            Console.getInstance().write(Property.getString("pic") + ": " + tempFile.getAbsolutePath());
                    files.increment();

                } else if (file.endsWith(FILE_EXTENSION_TO_MANIPULATE) && getConvertLivingImagesToVideo().isSelected()) {
                    putThmFiles(tempDir, tempFile);

                    //              Console.getInstance().write(Property.getString("vid") + ": " + tempFile.getAbsolutePath());
                    //+ String.format(": %1$20s TO %2$20s", origin, newName));
                    files.increment();

                } else if (file.endsWith(FILE_EXTENSION_COMPRESS[0]) && getConvertLivingImagesToVideo().isSelected()) {
                    putTnlFiles(tempDir, tempFile);
                    //                Console.getInstance().write("tnl-" + Property.getString("file") + ": " + tempFile.getAbsolutePath());
                    files.increment();

                } else if (file.endsWith(FILE_EXTENSION_COMPRESS[1]) && getArchiveRefocus().isSelected()) {
                    putNarFiles(tempDir, tempFile);
                    //                  Console.getInstance().write("nar-" + Property.getString("file") + ": " + tempFile.getAbsolutePath());
                    files.increment();

                } else if (file.endsWith(FILE_EXTENSION_ARCHIVE[0]) && getRestoreLivingImages().isSelected()) {
                    putTnlArchives(tempDir, tempFile);
                    //                    Console.getInstance().write("tnl-" + Property.getString("archive") + ": " + tempFile.getAbsolutePath());
                    files.increment();

                } else if (file.endsWith(FILE_EXTENSION_ARCHIVE[1]) && getUnarchiveRefocus().isSelected()) {
                    putNarArchives(tempDir, tempFile);
                    //                      Console.getInstance().write("nar-" + Property.getString("archive") + ": " + tempFile.getAbsolutePath());
                    files.increment();

                } else if (file.endsWith(FILE_EXTENSION_VIDEO) && getRestoreLivingImages().isSelected()) {
                    putVideos(tempDir, tempFile);
                    //                        Console.getInstance().write(Property.getString("vid") + ": " + tempFile.getAbsolutePath());
                    files.increment();
                }
            }
        }
        analyse.finish();

        int operations = 0;
        if (!getDeleteNone().isSelected())
            operations++;
        if (!getDoNothingOnLivingImages().isSelected())
            operations++;
        if (!getDoNothingOnRefocus().isSelected())
            operations++;

        Progress operationsProgress = CommonProgressMonitor.INSTANCE.createProgress(Property.getString("operations"), false, false, false, false);
        operationsProgress.setMax(operations);
        operationsProgress.start();

            /* Startet Bearbeitung */
        if (!getDeleteNone().isSelected()) {
            Progress cleanUp = CommonProgressMonitor.INSTANCE.createProgress(Property.getString("dirs"), false, false, false, false);
            cleanUp.setMax(getLowResPictures().keySet().size());
            cleanUp.start();


            for (File tempDir : getLowResPictures().keySet()) {

                Progress cleanUp2 = CommonProgressMonitor.INSTANCE.createProgress(Property.getString("files"), false, false, false, false);
                cleanUp2.setMax(getLowResPictures().get(tempDir).size());
                cleanUp2.start();

                // Loesche alle doppelten Bilder unterschiedlicher Aufloesung
                for (File picture : getLowResPictures().get(tempDir)) {
                    String clearedSourceName = picture.getName();
                    int fileExtensionIndex = clearedSourceName.lastIndexOf(".");
                    clearedSourceName = clearedSourceName.substring(0, fileExtensionIndex);

                    String compareName = clearedSourceName + FILE_EXTENSION_HIGHRES;

                    if (getHighResPictures().get(tempDir) != null)
                        for (File highResPicture : getHighResPictures().get(tempDir)) {
                            if (highResPicture.getName().equals(compareName)) {

                                // Loescht niedrige Aufloesung
                                File deletePicture = picture;

                                // Loescht hohe Aufloesung
                                if (getDeleteHighRes().isSelected())
                                    deletePicture = highResPicture;

                                deletePicture.delete();
                                Console.getInstance().write(Property.getString("picDeleted") + ": " + deletePicture.getAbsolutePath());
                                break;
                            }
                        }
                }

                cleanUp2.finish();
                cleanUp.increment();
            }

            cleanUp.finish();
            operationsProgress.increment();
        }

        // Benenne Videodateien um und markiere tnl Dateien
        if (getConvertLivingImagesToVideo().isSelected()) {
            Progress cleanUp = CommonProgressMonitor.INSTANCE.createProgress(Property.getString("dirs"), false, false, false, false);
            cleanUp.setMax(getThmFiles().keySet().size());
            cleanUp.start();

            for (File tempDir : getThmFiles().keySet()) {
                Progress cleanUp2 = CommonProgressMonitor.INSTANCE.createProgress(Property.getString("files"), false, false, false, false);
                cleanUp2.setMax(getThmFiles().get(tempDir).size());
                cleanUp2.start();

                if (getThmFiles().get(tempDir) != null)
                    for (File video : getThmFiles().get(tempDir)) {
                        // Bestimme Dateinamen
                        String origin = video.getName();

                        // Bilde neuen Namen
                        int extensionIndex = origin.lastIndexOf(".");
                        String newName = video.getName().substring(0, extensionIndex);
                        if (!newName.endsWith(FILE_EXTENSION_VIDEO))
                            newName += FILE_EXTENSION_VIDEO;

                        // �ndere Dateinamen
                        video.renameTo(new File(buildPath(tempDir.getAbsolutePath(), newName)));
                        Console.getInstance().write(Property.getString("convert") + String.format(": %1$20s TO %2$20s", origin, newName));

                        cleanUp2.increment();
                    }
                cleanUp2.finish();

                // Erstelle tnl-Archiv
                File tnlArchiv = new File(buildPath(tempDir.getAbsolutePath(), "livingImages.tna"));
                tnlArchiv.createNewFile();

                packZip(tnlArchiv, getTnlFiles().get(tempDir));
                deleteFiles(getTnlFiles().get(tempDir));

            }

            cleanUp.finish();
            operationsProgress.increment();

        } else if (getRestoreLivingImages().isSelected() && !getTnlArchives().isEmpty()) {
            Progress cleanUp = CommonProgressMonitor.INSTANCE.createProgress(Property.getString("dirs"), false, false, false, false);
            cleanUp.setMax(getTnlArchives().keySet().size());
            cleanUp.start();

            for (File tempDir : getTnlArchives().keySet()) {
                for (File archive : getTnlArchives().get(tempDir)) {

                    LinkedList<File> unarchivedFiles = unpackIt(archive, archive.getParent());
                    archive.delete();

                    Progress cleanUp2 = CommonProgressMonitor.INSTANCE.createProgress(Property.getString("files"), false, false, false, false);
                    cleanUp2.setMax(unarchivedFiles.size());
                    cleanUp2.start();

                    for (File unarchived : unarchivedFiles) {
                        String unarchivedName = unarchived.getName();
                        int indexOfFileExtension = unarchivedName.lastIndexOf(".");
                        unarchivedName = unarchivedName.substring(0, indexOfFileExtension);

                        if (getVideos().get(tempDir) != null)
                            for (File video : getVideos().get(tempDir)) {
                                if (video.getName().equals(unarchivedName)) {
                                    String origin = video.getName();
                                    video.renameTo(new File(buildPath(tempDir.getAbsolutePath(), origin + FILE_EXTENSION_TO_MANIPULATE)));
                                    Console.getInstance().write(Property.getString("convert") + String.format(": %1$20s TO %2$20s", origin, video.getName() + FILE_EXTENSION_TO_MANIPULATE));
                                    break;
                                }
                            }

                        cleanUp2.increment();
                    }
                    cleanUp2.finish();
                }
            }

            cleanUp.finish();
            operationsProgress.increment();
        }

        // Packe alle Refocus-Dateien
        if (getArchiveRefocus().isSelected() && !getNarFiles().isEmpty()) {
            Progress cleanUp = CommonProgressMonitor.INSTANCE.createProgress(Property.getString("dirs"), false, false, false, false);
            cleanUp.setMax(getNarFiles().keySet().size());
            cleanUp.start();

            for (File tempDir : getNarFiles().keySet()) {
                // Erstelle tnl-Archiv
                File archiv = new File(buildPath(tempDir.getAbsolutePath(), "refocus.naa"));
                archiv.createNewFile();

                packZip(archiv, getNarFiles().get(tempDir));
                deleteFiles(getNarFiles().get(tempDir));

                cleanUp.increment();
            }

            cleanUp.finish();
            operationsProgress.increment();

        } else if (getUnarchiveRefocus().isSelected() && !getNarArchives().isEmpty()) {
            Progress cleanUp = CommonProgressMonitor.INSTANCE.createProgress(Property.getString("dirs"), false, false, false, false);
            cleanUp.setMax(getNarArchives().keySet().size());
            cleanUp.start();

            for (File tempDir : getNarArchives().keySet()) {

                for (File archive : getNarArchives().get(tempDir)) {
                    unpackIt(archive, archive.getParent());
                    archive.delete();

                    cleanUp.increment();
                }
            }

            cleanUp.finish();
            operationsProgress.increment();
        }

        operationsProgress.finish();
        getStart().setEnabled(true);
        Console.getInstance().write(Property.getString("finish"));
    }

    private JTextField getPath() {
        if (path == null) {
            path = new JTextField();
            path.setEditable(false);
            path.setText(new File("test").getParent());
        }
        return path;
    }

    private JRadioButton getDeleteHighRes() {
        if (deleteHighRes == null) {
            deleteHighRes = new JRadioButton(Property.getString("deleteHigh"));
        }
        return deleteHighRes;
    }

    private JRadioButton getDeleteLowRes() {
        if (deleteLowRes == null) {
            deleteLowRes = new JRadioButton(Property.getString("deleteLow"));
            deleteLowRes.setSelected(true);
        }
        return deleteLowRes;
    }

    private JRadioButton getDeleteNone() {
        if (deleteNone == null) {
            deleteNone = new JRadioButton(Property.getString("deleteNone"));
        }
        return deleteNone;
    }

    private JRadioButton getConvertLivingImagesToVideo() {
        if (convertLivingImagesToVideo == null) {
            convertLivingImagesToVideo = new JRadioButton(Property.getString("converte"));
            convertLivingImagesToVideo.setSelected(true);
        }
        return convertLivingImagesToVideo;
    }

    private JRadioButton getRestoreLivingImages() {
        if (restoreLivingImages == null) {
            restoreLivingImages = new JRadioButton(Property.getString("restore"));
        }
        return restoreLivingImages;
    }

    private JRadioButton getDoNothingOnLivingImages() {
        if (doNothingOnLivingImages == null) {
            doNothingOnLivingImages = new JRadioButton(Property.getString("doNothing"));
        }
        return doNothingOnLivingImages;
    }

    private JCheckBox getSubfolders() {
        if (subfolders == null) {
            subfolders = new JCheckBox(Property.getString("subdirs"));
        }
        return subfolders;
    }

    private JRadioButton getDoNothingOnRefocus() {
        if (doNothingOnRefocus == null) {
            doNothingOnRefocus = new JRadioButton(Property.getString("doNothing"));
        }
        return doNothingOnRefocus;
    }

    private JRadioButton getArchiveRefocus() {
        if (archiveRefocus == null) {
            archiveRefocus = new JRadioButton(Property.getString("archivate"));
            archiveRefocus.setSelected(true);
        }
        return archiveRefocus;
    }

    private JRadioButton getUnarchiveRefocus() {
        if (unarchiveRefocus == null) {
            unarchiveRefocus = new JRadioButton(Property.getString("restore"));
        }
        return unarchiveRefocus;
    }

    private JButton getSelectDir() {
        if (selectDir == null) {
            selectDir = new JButton("...");
            selectDir.addActionListener(getButtonActions());
        }
        return selectDir;
    }


    private JFrame getFrame() {
        if (frame == null) {
            frame = new JFrame(Property.getString("titel"));
            frame.setLayout(new BorderLayout(5, 5));
        }
        return frame;
    }

    private ActionListener getButtonActions() {
        if (buttonActions == null) {
            buttonActions = e -> {
                if (e.getSource() == getSelectDir()) {
                    JFileChooser jf = new JFileChooser();
                    jf.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

                    int accept = jf.showOpenDialog(getFrame());

                    if (accept == JFileChooser.APPROVE_OPTION && jf.getSelectedFile().isDirectory()) {
                        path.setText(jf.getSelectedFile().getAbsolutePath());
                        directory = jf.getSelectedFile();

                        getStart().setEnabled(true);
                    }

                } else if (e.getSource() == getStart()) {
                    new Thread(() -> {
                        try {
                            getStart().setEnabled(false);
                            editFiles(directory);
                        } catch (IOException e1) {
                            BugReport.recordBug(e1, false);
                        }
                    }).start();

                }
            };
        }
        return buttonActions;
    }

    private HashMap<File, LinkedList<File>> getLowResPictures() {
        if (lowResPictures == null) {
            lowResPictures = new HashMap<>();
        }
        return lowResPictures;
    }

    private void putLowResPictures(File dir, File f) {
        if (getLowResPictures().get(dir) == null)
            getLowResPictures().put(dir, new LinkedList<>());

        getLowResPictures().get(dir).add(f);
    }

    private HashMap<File, LinkedList<File>> getHighResPictures() {
        if (highResPictures == null) {
            highResPictures = new HashMap<>();
        }
        return highResPictures;
    }

    private void putHighResPictures(File dir, File f) {
        if (getHighResPictures().get(dir) == null)
            getHighResPictures().put(dir, new LinkedList<>());

        getHighResPictures().get(dir).add(f);
    }

    private HashMap<File, LinkedList<File>> getTnlFiles() {
        if (tnlFiles == null) {
            tnlFiles = new HashMap<>();
        }
        return tnlFiles;
    }

    private void putTnlFiles(File dir, File f) {
        if (getTnlFiles().get(dir) == null)
            getTnlFiles().put(dir, new LinkedList<>());

        getTnlFiles().get(dir).add(f);
    }

    private HashMap<File, LinkedList<File>> getThmFiles() {
        if (thmFiles == null) {
            thmFiles = new HashMap<>();
        }
        return thmFiles;
    }

    private void putThmFiles(File dir, File f) {
        if (getThmFiles().get(dir) == null)
            getThmFiles().put(dir, new LinkedList<>());

        getThmFiles().get(dir).add(f);
    }

    private HashMap<File, LinkedList<File>> getNarFiles() {
        if (narFiles == null) {
            narFiles = new HashMap<>();
        }
        return narFiles;
    }

    private void putNarFiles(File dir, File f) {
        if (getNarFiles().get(dir) == null)
            getNarFiles().put(dir, new LinkedList<>());

        getNarFiles().get(dir).add(f);
    }

    private HashMap<File, LinkedList<File>> getVideos() {
        if (videos == null) {
            videos = new HashMap<>();
        }
        return videos;
    }

    private void putVideos(File dir, File f) {
        if (getVideos().get(dir) == null)
            getVideos().put(dir, new LinkedList<>());

        getVideos().get(dir).add(f);
    }

    private HashMap<File, LinkedList<File>> getTnlArchives() {
        if (tnlArchives == null) {
            tnlArchives = new HashMap<>();
        }
        return tnlArchives;
    }

    private void putTnlArchives(File dir, File f) {
        if (getTnlArchives().get(dir) == null)
            getTnlArchives().put(dir, new LinkedList<>());

        getTnlArchives().get(dir).add(f);
    }

    private HashMap<File, LinkedList<File>> getNarArchives() {
        if (narArchives == null) {
            narArchives = new HashMap<>();
        }
        return narArchives;
    }

    private void putNarArchives(File dir, File f) {
        if (getNarArchives().get(dir) == null)
            getNarArchives().put(dir, new LinkedList<>());

        getNarArchives().get(dir).add(f);
    }

    private JButton getStart() {
        if (start == null) {
            start = new JButton(Property.getString("start"));
            start.addActionListener(getButtonActions());
            start.setEnabled(false);
        }
        return start;
    }
}
